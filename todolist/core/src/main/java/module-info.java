module todolist.core {
    requires transitive com.fasterxml.jackson.databind;

    exports todolist.core;
    exports todolist.json;
}
